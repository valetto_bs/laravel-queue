<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/{path?}', 'app');

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/publisher', function () {
//     return view('publisher');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::prefix('car')->group(function () {
	
// 	Route::get('/','CarController@index');
	
// 	Route::get('add','CarController@create');
// 	Route::post('add','CarController@store');

// 	Route::get('edit/{id}','CarController@edit');
// 	Route::post('edit/{id}','CarController@update');
// 	Route::delete('{id}','CarController@destroy');

// });




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
