<?php

use Illuminate\Http\Request;
use App\User;
use App\OrderLog;
use App\Http\Resources\Users as UserResource;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users/{id}', function (int $id) {
    $data = OrderLog::where('user_id', $id )->with('buy_trades', 'sell_trades')->get();
    return $data->toarray();
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/users', function () {
//     return UserResource::collection(User::all());
// });

// Route::get('/users/{user}', function () {
//     return User::find(1)->get;
// });

// Route::get('/users/{id}', function (int $id) {
//     // DB::enableQueryLog();
//     $data = User::where('user_id', $id )->with('orders', 'buy_trades', 'sell_trades')->get();
//     // $queries = DB::getQueryLog();
//     // print_r($queries);
//     return $data->toarray();
// });

// Route::get('/users/{user}', function (App\User $user) {
//     return $user->email;
// });

