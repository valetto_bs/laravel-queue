<?php

// use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\User::class, 50)->create();

        foreach (range(1,100) as $key => $value) {
            $faker = Faker::create();
        //     // DB::table('users')->insert([
        //     //     'name' => $faker->name,
        //     //     'email' => $faker->email,
        //     //     'password' => bcrypt('password'),
        //     // ]);

            $user_arr = [];
            $user_arr['user_id'] = $key+1;
            $user_arr['name'] = $faker->name;
            $user_arr['email'] = $faker->email;
            $user_arr['password'] = bcrypt('password');

            $user = User::create($user_arr);
            $user->save();

        //     // $user = new User;
        //     // $user->name = $faker->name;
        //     // $user->email = $faker->email;
        //     // $user->password = bcrypt('password');
        //     // // $user->fill($user_arr);
        //     // $user->save();
        }
    }
}
