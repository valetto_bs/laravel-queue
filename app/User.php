<?php

namespace App;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use App\OrderLog;
use App\TradeLogs;
use Moloquent;

class User extends Moloquent
{
    // use Notifiable;
	protected $collection = 'users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    // protected $primaryKey = 'user_id';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    // protected $attributes = [
    //     'user_id' => 1,
    // ];

    // public function getRouteKeyName()
    // {
    //     return 'user_id';
    // }

    public function orders()
    {
        return $this->hasMany('App\OrderLog', 'user_id', 'user_id');
    }

    // public function buy_trades()
    // {
    //     return $this->hasManyThrough(
    //         'App\TradeLogs',
    //         'App\OrderLog',
    //         'user_id',
    //         '_id',
    //         'user_id',
    //         'buy_id'
    //     );
    // }

    // public function sell_trades()
    // {
    //     return $this->hasManyThrough(
    //         'App\OrderLog',
    //         'App\TradeLogs',
    //         'user_id', 
    //         'sell_id', 
    //         'user_id',
    //         '_id'
    //     );
    // }
}
