<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
// use Jenssegers\Mongodb\Eloquent\Model;
use App\TradeLogs;
use App\User;
use Moloquent;

class OrderLog extends Moloquent
{
	protected $collection = 'order_logs';
    
    protected $fillable = [
    	'_id', 'user_id', 'type', 'currency_code','amount','status'
    ];

    protected $attributes = [
        'status' => 0
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'user_id');
    }

    public function buy_trades()
    {
        return $this->hasOne('App\TradeLogs', 'buy_id', '_id');
    }

    public function sell_trades()
    {
        return $this->hasOne('App\TradeLogs', 'sell_id', '_id');
    }
}
