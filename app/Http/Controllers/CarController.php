<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Car;

class CarController extends Controller
{
	public function index()
	{
		$cars=Car::all();
        return view('car/index',compact('cars'));
	}

    public function create()
    {
        return view('car/carcreate');
    }

    public function store(Request $request)
    {
        $car = new Car();
        $car->carcompany = $request->get('carcompany');
        $car->model = $request->get('model');
        $car->price = $request->get('price');        
        $car->save();

        return redirect('car/')->with('success', 'Car has been successfully added');
    }
}
