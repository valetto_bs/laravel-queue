<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\OrderLog;

class OrderController extends Controller
{
	private $type;
	private $status;
	private $currency_code;
	private $amount;

	public function __construct()
    {
        parent::__construct();
    }

    public function create($type,$currency_code,$amount)
    {	
    	$order_log = new OrderLog();
    	$order_log->type = $type;
        $order_log->currency_code = $currency_code;
        $order_log->amount = $amount;        
        $order_log->save();
    }

}
