<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

use Moloquent;

class TradeLogs extends Moloquent
{
    protected $collection = 'trade_logs';
    
    protected $fillable = [
        '_id','currency_code','amount','buy_id','sell_id'
    ];
}
