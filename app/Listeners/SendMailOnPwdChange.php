<?php

namespace App\Listeners;

use App\Events\ForgotPwd;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailOnForgotPwd implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function onChange($events)
    {
        echo 'inside password change method';
    }

    /**
     * Handle the event.
     *
     * @param  PwdChange  $event
     * @return void
     */
    public function handle(ForgotPwd $event)
    {
        $events->listen(
            'App\Events\ForgotPwd',
            'App\Listeners\SendMailOnForgotPwd@onChange'
        );
    }
}
