<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\OrderLog;

class ProcessOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $type;
    protected $currency_code;
    protected $amount;

    /**
     Create a new job instance.
    
     @return void
     */
    public function __construct($order)
    {
        $this->type = $order->type;
        $this->currency_code = $order->currency_code;
        $this->amount = $order->amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order_log = new OrderLog;

        $order_log->type =  $this->type;
        $order_log->currency_code = $this->currency_code;
        $order_log->amount = $this->amount;
        $order_log->user_id = $this->user_id;
        // sleep(1);
        $order_log->save();
    }
}
