<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// use App\OrderLog;
use App\TradeLogs;

class MatchOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $sell_id;
    protected $buy_id;
    protected $currency_code;
    protected $amount;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($buy_id, $sell_id, $currency_code, $amount)
    {
        $this->buy_id = $buy_id;
        $this->sell_id = $sell_id;
        $this->currency_code = $currency_code;
        $this->amount = $amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $updateOrderLog = OrderLog::whereIn('_id',[$this->buy_id,$this->sell_id])
        //   ->update(['status' => 1]);

        // if ( $updateOrderLog ) {
        //     echo 'matched';
            // $trade_log = new TradeLogs;
            // $trade_log->currency_code = $this->currency_code;
            // $trade_log->amount = $this->amount;
            // $trade_log->buy_id =  $this->buy_id;
            // $trade_log->sell_id =  $this->sell_id;
            // $trade_log->save();

            $trade_arr = [];
            $trade_arr['currency_code'] = $this->currency_code;
            $trade_arr['amount'] = $this->amount;
            $trade_arr['buy_id'] = $this->buy_id;
            $trade_arr['sell_id'] = $this->sell_id;

            $trade_log = new TradeLogs;
            $trade_log->fill($trade_arr);
            $trade_log->save();
        // }

    }
}
