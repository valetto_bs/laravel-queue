<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Jobs\MatchOrderJob;

use App\OrderLog;

class MatchOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:match';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Matcher';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            echo 'matcher is running...';
        while(1) {
            echo '.';
            $buyOrders = OrderLog::where('status', 0)
                       ->where('type','buy')
                       ->orderBy('created_at', 'asc')
                       ->get();

            $sellOrders = OrderLog::where('status', 0)
                       ->where('type','sell')
                       ->orderBy('created_at', 'asc')
                       ->get();  

            if ( count($buyOrders) > 0 && count($sellOrders) > 0 ){  
                
                foreach ($buyOrders as $buyOrder) {
                    foreach ($sellOrders as $key => $sellOrder) {
                        if ( $buyOrder->amount === $sellOrder->amount && $buyOrder->currency_code == $sellOrder->currency_code ) {
                            unset($sellOrders[$key]);
                            $updateOrderLog = OrderLog::whereIn('_id',[$buyOrder->_id,$sellOrder->_id])
                            ->update(['status' => 1]);
                            echo ':';
                            if ( $updateOrderLog ) {
                                // MatchOrderJob::dispatch($buyOrder->id, $sellOrder->id, $buyOrder->currency_code, $buyOrder->amount)->onConnection('redis')->onQueue('match');
                                MatchOrderJob::dispatch($buyOrder->id, $sellOrder->id, $buyOrder->currency_code, $buyOrder->amount)->onQueue('match');
                                break;
                            }
                        }
                    }
                }
            }
        }

    }
}
