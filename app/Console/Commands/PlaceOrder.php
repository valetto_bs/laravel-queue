<?php

namespace App\Console\Commands;

use App\Events\NewOrderNotification;
use Illuminate\Console\Command;

// use App\Jobs\ProcessOrder;

use App\OrderLog;

class PlaceOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:place';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Placing random orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currArray = [
            'btc','tusd','xrp'
        ];

        $typeArr = ['buy','sell'];

        $orderCount = 1000;
        $prevType = "";

        for ($i=0; $i < $orderCount; $i++) { 
            sleep(1);
            $type = array_rand($typeArr);
            $currency_code = array_rand($currArray);
            $amount = rand(1,100);
            $user_id = rand(1,100);

            $order_arr = [];

            $order_arr['type'] = $typeArr[$type];
            $order_arr['currency_code'] = $currArray[$currency_code];
            $order_arr['amount'] = $amount;
            $order_arr['user_id'] = $user_id;
            $order_log = OrderLog::create($order_arr);

            // broadcast(new NewOrderNotification($order_log));
            event(new NewOrderNotification($order_log));
            $prevType = $type;
        }

        echo "$orderCount orders placed.";
    }
}
