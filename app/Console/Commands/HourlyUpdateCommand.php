<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\Mail\HourlyUpdate;
use Illuminate\Support\Facades\Mail;


class HourlyUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hour:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::all();

        foreach ($user as $a) {
            Mail::to($a['email'])->send(new HourlyUpdate($a));
        }
        
        $this->info('Hourly Update has been send successfully');
    }
}
