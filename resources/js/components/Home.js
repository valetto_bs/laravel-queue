import React, { Component } from 'react';
import Order from '../charts/Order';

export default class Home extends Component {
    componentDidMount(){
        document.title = "Home"
    }

    render() {
        
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Home Component</div>

                            <div className="card-body">I'm an home component!</div>
                            
                            <Order />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
