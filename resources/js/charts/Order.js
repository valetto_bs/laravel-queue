import React, { Component } from 'react';
import CanvasJSReact from '../canvas/canvasjs.react';
import CanvasJS from '../canvas/canvasjs.min';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
const moment = require('moment');
import Echo from "laravel-echo";
const io = window.io = require('socket.io-client');
const echo = window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

var dataPoints1 = [];
var dataPoints2 = [];
var yValue1 = 0;
var yValue2 = 0;
var xValue = 0;
var time;
 
export default class Order extends Component {
	constructor() {
		super();
		this.updateChart = this.updateChart.bind(this);		
	}
	componentDidMount(){
		echo.channel('orders-channel')
		.listen('.order.publisher', (e) => {
			this.updateChart(e.data);
		});
	}
    componentWillUnmount() {
		echo.leaveChannel('orders-channel');
        this.chart.destroy();
        if(this.props.onRef)
            this.props.onRef(undefined);
    }    
	
	updateChart(data) {
		console.log(data);

		time = moment.utc(data.created_at).local().format();
		xValue = time;

		if(data.type == "buy") {
			yValue1 = data.amount;
			dataPoints1.push({
			x: new Date(xValue),
			y: yValue1
			});
		}
		else if(data.type == "sell") {
			yValue2 = data.amount;
			dataPoints2.push({
			x: new Date(xValue),
			y: yValue2
			});
		}

		this.chart.options.data[0].legendText = " Buy - " + yValue1 + " INR";
		this.chart.options.data[1].legendText = " Sell - " + yValue2 + " INR";
		this.chart.render();
	}
	render() {
		const options = {
			zoomEnabled: true,
			theme: "light2",
			title: {
				text: "Buy vs Sell"
			},
			axisX: {
				title: "Live Data",
				labelFormatter: function (e) {
					return CanvasJS.formatDate( e.value, "DD MMM hh:mm:ss");
				},
			},
			axisY:{
				suffix: " INR",
				includeZero: false
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor:"pointer",
				verticalAlign: "top",
				fontSize: 18,
				fontColor: "dimGrey",
				itemclick : function(e){
					if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
						e.dataSeries.visible = false;
					}
					else {
						e.dataSeries.visible = true;
					}
					e.chart.render();
				}
			},
			data: [
				{
					type: "spline",
					// xValueFormatString: "#,##0 seconds",
					yValueFormatString: "#,##0 INR",
					showInLegend: true,
					name: "Buy",
					color: "green",  
					dataPoints: dataPoints1
				},
				{
					type: "spline",
					// xValueFormatString: "#,##0 seconds",
					yValueFormatString: "#,##0 INR",
					showInLegend: true,
					name: "Sell" ,
					color: "red",  
					dataPoints: dataPoints2
				}
			]
		}
		
		return (
		<div>
			<CanvasJSChart options = {options} 
				onRef={ref => this.chart = ref}
			/>
		</div>
		);
	}
}